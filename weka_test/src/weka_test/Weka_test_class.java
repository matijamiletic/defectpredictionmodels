package weka_test;

import weka_test.MultiColumnCsvSortExtended;

import weka.core.Attribute;
import weka.core.Instance;

import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToNominal;
import weka.filters.unsupervised.attribute.Remove;
import weka.gui.visualize.PlotData2D;
import weka.gui.visualize.ThresholdVisualizePanel;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.classifiers.evaluation.output.prediction.CSV;
import weka.attributeSelection.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.awt.BorderLayout;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.NumberFormat;

//import weka.core.converters.ConverterUtils.DataSource;

//reference 
//https://weka.wikispaces.com/Use+WEKA+in+your+Java+code
//https://ianma.wordpress.com/2010/01/16/weka-with-java-eclipse-getting-started/

public class Weka_test_class {

	static Instances test = null;
	static Instances train = null;
	static String[] arffFiles = new String[2];
	static String arffFile_n = null;
	static String arffFile_n_1 = null;
	static Classifier cls = null;
	static Hashtable<String, String> resultTable = null;
	static StringBuilder tmpBuffer = null;
	static double[] bug_cnt = null;
	static double[] bug_cnt_avg = null;
	static double[] LOC_avg = null;
	static Map<String, String> areaUnderCurve = new HashMap<String, String>();
	static int[] sortIndexes = {0,0};
	static double GM;
	static double AUC;
	
	public static void main(String[] args) throws Exception {
		
		//run program with two file names as input arguments
		//first argument - release n, second argument - release n-1
	    if (args.length != 2) {
	      System.out.println("\nPlease enter two input files\n");
	      System.exit(1);
	    }
	    
	    
	   	String filename1 = args[0] + ".csv";
	   	String filename2 = args[1] + ".csv";
	    
	   	System.out.println("Input files:");
		System.out.println(filename1);
		System.out.println(filename2);
		
		//test release file name
		arffFile_n = CSVtoArff(filename1);
		//train release file name
		arffFile_n_1 = CSVtoArff(filename2);
		
		//test data
		//arffFile_n = "D:/FAKS/UPI java/weka_test/predictFileHidden/JDT/writeOldestResults_JDT_R3_2_hidden.arff";
		//arffFile_n_1 = "D:/FAKS/UPI java/weka_test/predictFileHidden/JDT/writeOldestResults_JDT_R3_1.arff";
		
		//make test instance
		test = createInstanceFromFile(arffFile_n); //JDT_2
		//make train instance
		train = createInstanceFromFile(arffFile_n_1); //JDT_1
		
		//get sorting indexes
		int bugCntBinIndex = test.attribute("bug_cnt_bin").index();
		int DDindex = test.attribute("DD").index();
		sortIndexes[0] = bugCntBinIndex;
		sortIndexes[1] = DDindex;
		//save bug_cnt attribute values
		bug_cnt = dataValuesAsArray(test, "bug_cnt");
		
		//save bug_cnt average attribute values
		bug_cnt_avg = dataValuesAsArray(test, "bug_cnt_avg");
		
		//save LOC average attribute values
		LOC_avg = dataValuesAsArray(test, "LOC_avg");
				
		//last column to nominal
		test = filterDataToNominal(test);
		train = filterDataToNominal(train);
		
		//remove first column containing file name
		test = filterDataRemoveAttributes(test, "File");
		train = filterDataRemoveAttributes(train, "File");
				
		//remove column containing DD
		test = filterDataRemoveAttributes(test, "DD");
		train = filterDataRemoveAttributes(train, "DD");
		
		//remove column containing bug count
		test = filterDataRemoveAttributes(test, "bug_cnt");
		train = filterDataRemoveAttributes(train, "bug_cnt");
		
		test = filterDataRemoveAttributes(test, "Absolute difference bug_cnt");
		train = filterDataRemoveAttributes(train, "Absolute difference bug_cnt");
				
		test = filterDataRemoveAttributes(test, "Relative difference bug_cnt");
		train = filterDataRemoveAttributes(train, "Relative difference bug_cnt");
		
		test = filterDataRemoveAttributes(test, "bug_cnt_avg");
		train = filterDataRemoveAttributes(train, "bug_cnt_avg");
		
		//remove column containing average LOC		
		test = filterDataRemoveAttributes(test, "LOC_avg");
		train = filterDataRemoveAttributes(train, "LOC_avg");
		
		//remove column containing ClassFound column
		test = filterDataRemoveAttributes(test, "ClassFound");
		train = filterDataRemoveAttributes(train, "ClassFound");
		
		//remove column containing Version column
		test = filterDataRemoveAttributes(test, "Version");
		train = filterDataRemoveAttributes(train, "Version");

		/*
		//start - remove attributes with small entropy - JDT
		test = filterDataRemoveAttributes(test, "Absolute difference MOD");
		train = filterDataRemoveAttributes(train, "Absolute difference MOD");
		
		test = filterDataRemoveAttributes(test, "Relative difference NSUP");
		train = filterDataRemoveAttributes(train, "Relative difference NSUP");
		
		test = filterDataRemoveAttributes(test, "Relative difference MPC");
		train = filterDataRemoveAttributes(train, "Relative difference MPC");
		
		test = filterDataRemoveAttributes(test, "Relative difference EXT");
		train = filterDataRemoveAttributes(train, "Relative difference EXT");
		
		test = filterDataRemoveAttributes(test, "MOD");
		train = filterDataRemoveAttributes(train, "MOD");
		
		test = filterDataRemoveAttributes(test, "Relative difference MOD");
		train = filterDataRemoveAttributes(train, "Relative difference MOD");
		//end - remove attributes with small entropy - JDT
		*/
		
		/*
		//start - remove attributes with small entropy - PDE
		test = filterDataRemoveAttributes(test, "Relative difference CLOC");
		train = filterDataRemoveAttributes(train, "Relative difference CLOC");
		
		test = filterDataRemoveAttributes(test, "FOUT");
		train = filterDataRemoveAttributes(train, "FOUT");
		
		test = filterDataRemoveAttributes(test, "Relative difference CCML");
		train = filterDataRemoveAttributes(train, "Relative difference CCML");
		
		test = filterDataRemoveAttributes(test, "Absolute difference MOD");
		train = filterDataRemoveAttributes(train, "Absolute difference MOD");
		
		test = filterDataRemoveAttributes(test, "Absolute difference MI");
		train = filterDataRemoveAttributes(train, "Absolute difference MI");
		
		test = filterDataRemoveAttributes(test, "Absolute difference HCLOC");
		train = filterDataRemoveAttributes(train, "Absolute difference HCLOC");
		
		test = filterDataRemoveAttributes(test, "Relative difference MI");
		train = filterDataRemoveAttributes(train, "Relative difference MI");
		
		test = filterDataRemoveAttributes(test, "Relative difference DIT");
		train = filterDataRemoveAttributes(train, "Relative difference DIT");
		
		test = filterDataRemoveAttributes(test, "Relative difference NSUP");
		train = filterDataRemoveAttributes(train, "Relative difference NSUP");
		
		test = filterDataRemoveAttributes(test, "Absolute difference HCWORD");
		train = filterDataRemoveAttributes(train, "Absolute difference HCWORD");
		
		test = filterDataRemoveAttributes(test, "Relative difference R_R");
		train = filterDataRemoveAttributes(train, "Relative difference R_R");
		
		test = filterDataRemoveAttributes(test, "Relative difference NSUB");
		train = filterDataRemoveAttributes(train, "Relative difference NSUB");
		
		test = filterDataRemoveAttributes(test, "Absolute difference CBO");
		train = filterDataRemoveAttributes(train, "Absolute difference CBO");
		
		test = filterDataRemoveAttributes(test, "Relative difference FOUT");
		train = filterDataRemoveAttributes(train, "Relative difference FOUT");
		
		test = filterDataRemoveAttributes(test, "Absolute difference SIX");
		train = filterDataRemoveAttributes(train, "Absolute difference SIX");
		
		test = filterDataRemoveAttributes(test, "Relative difference CWORD");
		train = filterDataRemoveAttributes(train, "Relative difference CWORD");
		
		test = filterDataRemoveAttributes(test, "Absolute difference INTR");
		train = filterDataRemoveAttributes(train, "Absolute difference INTR");
		
		test = filterDataRemoveAttributes(test, "NSUB");
		train = filterDataRemoveAttributes(train, "NSUB");
		
		test = filterDataRemoveAttributes(test, "INTR");
		train = filterDataRemoveAttributes(train, "INTR");
		
		test = filterDataRemoveAttributes(test, "Absolute difference FOUT");
		train = filterDataRemoveAttributes(train, "Absolute difference FOUT");
		
		test = filterDataRemoveAttributes(test, "Absolute difference CWORD");
		train = filterDataRemoveAttributes(train, "Absolute difference CWORD");
		
		test = filterDataRemoveAttributes(test, "Relative difference F_IN");
		train = filterDataRemoveAttributes(train, "Relative difference F_IN");
		
		test = filterDataRemoveAttributes(test, "Relative difference HCWORD");
		train = filterDataRemoveAttributes(train, "Relative difference HCWORD");
		
		test = filterDataRemoveAttributes(test, "Absolute difference NSUB");
		train = filterDataRemoveAttributes(train, "Absolute difference NSUB");
		
		test = filterDataRemoveAttributes(test, "S_R");
		train = filterDataRemoveAttributes(train, "S_R");
		
		test = filterDataRemoveAttributes(test, "Relative difference S_R");
		train = filterDataRemoveAttributes(train, "Relative difference S_R");
		
		test = filterDataRemoveAttributes(test, "DIT");
		train = filterDataRemoveAttributes(train, "DIT");
		
		test = filterDataRemoveAttributes(test, "Absolute difference NSUP");
		train = filterDataRemoveAttributes(train, "Absolute difference NSUP");
		
		test = filterDataRemoveAttributes(test, "Relative difference HCLOC");
		train = filterDataRemoveAttributes(train, "Relative difference HCLOC");
		
		test = filterDataRemoveAttributes(test, "Absolute difference DIT");
		train = filterDataRemoveAttributes(train, "Absolute difference DIT");
		
		test = filterDataRemoveAttributes(test, "Absolute difference CLOC");
		train = filterDataRemoveAttributes(train, "Absolute difference CLOC");
		
		test = filterDataRemoveAttributes(test, "Absolute difference S_R");
		train = filterDataRemoveAttributes(train, "Absolute difference S_R");
		
		test = filterDataRemoveAttributes(test, "F_IN");
		train = filterDataRemoveAttributes(train, "F_IN");
		
		test = filterDataRemoveAttributes(test, "Relative difference BLOC");
		train = filterDataRemoveAttributes(train, "Relative difference BLOC");
		
		test = filterDataRemoveAttributes(test, "Relative difference LOC");
		train = filterDataRemoveAttributes(train, "Relative difference LOC");
		
		test = filterDataRemoveAttributes(test, "SIX");
		train = filterDataRemoveAttributes(train, "SIX");
		
		test = filterDataRemoveAttributes(test, "CBO");
		train = filterDataRemoveAttributes(train, "CBO");
		
		test = filterDataRemoveAttributes(test, "Absolute difference R_R");
		train = filterDataRemoveAttributes(train, "Absolute difference R_R");
		
		test = filterDataRemoveAttributes(test, "Absolute difference F_IN");
		train = filterDataRemoveAttributes(train, "Absolute difference F_IN");
		
		test = filterDataRemoveAttributes(test, "MOD");
		train = filterDataRemoveAttributes(train, "MOD");
		
		test = filterDataRemoveAttributes(test, "Relative difference MOD");
		train = filterDataRemoveAttributes(train, "Relative difference MOD");
		
		test = filterDataRemoveAttributes(test, "Relative difference HIER");
		train = filterDataRemoveAttributes(train, "Relative difference HIER");
		
		test = filterDataRemoveAttributes(test, "Relative difference INTR");
		train = filterDataRemoveAttributes(train, "Relative difference INTR");
		
		test = filterDataRemoveAttributes(test, "Relative difference CBO");
		train = filterDataRemoveAttributes(train, "Relative difference CBO");
		//end - remove attributes with small entropy - PDE
		*/
		
		/*if (train.equalHeaders(test)) System.out.println("equal");
		else System.out.println("not equal");
		
		System.exit(0);*/
		
		
		//Evaluating - test how good is the classifier/cluster
		evaluatingInstances();
		
		showCurveData("optimal", args[0]);
		showCurveData("worst", args[0]);
		showCurveData("predicted", "predictedFile");
		
		double optimalArea =  Double.parseDouble(areaUnderCurve.get("optimal"));
		double worstArea =  Double.parseDouble(areaUnderCurve.get("worst"));
		double predictedArea =  Double.parseDouble(areaUnderCurve.get("predicted"));
		
		double Popt = calculatePopt(optimalArea, worstArea, predictedArea );
		System.out.println("Popt metric " + Popt);
		
		String fileName = args[0] + "__" + args[1];
		addTableData("PDE_NaiveBayes", fileName, Popt, AUC, GM);

	}
	
	private static double calculatePopt(double optimalArea, double worstArea, double predictedArea) {
		
		return 1 - ( ( optimalArea - predictedArea ) / ( optimalArea - worstArea ) );
		
	}

	private static void areaUnderCurve(double[] x_axis, double[] y_axis, String model) {
		
		double sum = 0;

		for (int i=0; i < x_axis.length-1; i++){
			double c = ( y_axis[i] + y_axis[i+1] )/2*(x_axis[i+1] - x_axis[i]);
			sum += c;
		}
		
		areaUnderCurve.put(model, String.valueOf(sum));
		System.out.println("Area under curve " + model + ": " + sum);
	}

	private static void evaluatingInstances() throws Exception {
	
		//set bug_cnt as predicting metric in test instancce
		test.setClassIndex(test.attribute("bug_cnt_bin").index()); 
		//set bug_cnt as predicting metric in train instancce
	    train.setClassIndex(train.attribute("bug_cnt_bin").index()); 
		
	    //set RandomForest properties 
		//RandomForest rf = new RandomForest();
		//feature number in tree (variable number)
	    //rf.setNumFeatures(147);
	    //rf.buildClassifier(train);
		//System.out.println(rf.getNumFeatures() + " iteracija " + rf.getNumIterations());
	    //cls = rf;
	    
	    //set J48 properties
	    /*J48 j48 = new J48();
	    j48.buildClassifier(train);
	    cls = j48;*/
	    
	    //set classifier for evaluation
	        
	    /*weka.classifiers.functions.Logistic logistic = new weka.classifiers.functions.Logistic();
	    logistic.buildClassifier(train);
	    cls = logistic;*/
	    
	    NaiveBayes naivebayes = new NaiveBayes();
	    naivebayes.buildClassifier(train);
	    cls = naivebayes;
	    
	    evaluateModel();
	    
	    
	    //evaluateSingleInstance();
	    //preformCrossValidation(3, 1);
	    //crossValidationTest();
	    
	    //write additional data to predictedFile.csv
	    addValuesToPredictedFile();
	    
	}
	 
	
	private static void addTableData(String fileName, String rowTitle, double P_opt, double AUC, double GM) throws IOException {
		
		//BufferedReader bReader=null;
		BufferedWriter bWriter=null;
		final String lineSep=System.getProperty("line.separator");
		
	    try {
	       //File fileInput = new File("predictFile.csv");
	       File fileOutput = new File(fileName + ".csv");

	       //bReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileInput))) ;
	       
	       if(fileOutput.exists()) { 
	    	   bWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileOutput, true)));
		       bWriter.write(rowTitle+","+P_opt+","+AUC+","+GM+lineSep);
	       } else {
	    	   bWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileOutput, true)));
	    	   bWriter.write("fileName,P_opt,AUC,GM"+lineSep);
		       bWriter.write(rowTitle+","+P_opt+","+AUC+","+GM+lineSep);
	       }
	       
	       
	       /*line = bReader.readLine();
	       bWriter.write(line+",bug_cnt,bug_cnt_avg,LOC_avg,predicted_class"+lineSep);
	       
	       for ( line = bReader.readLine(); line != null && i<bug_cnt.length; line = bReader.readLine()) {
	    	   String predictedClassAttr = line.split(",")[2];
	    	   String predictedClass = predictedClassAttr.split(":")[1];
	    	   bWriter.write(line+","+bug_cnt[i]+","+bug_cnt_avg[i]+","+LOC_avg[i]+","+predictedClass+lineSep);
	    	   i++;
	    }*/

	    }catch(Exception e){
	        System.out.println(e);
	    }finally  {
	        if(bWriter!=null)
	        	bWriter.close();
	    }
		
	}
	
	private static void addValuesToPredictedFile() throws IOException {
		
		BufferedReader bReader=null;
		BufferedWriter bWriter=null;
		final String lineSep=System.getProperty("line.separator");
		
	    try {
	       File fileInput = new File("predictFile.csv");
	       File fileOutput = new File("predictedFile.csv");

	       bReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileInput))) ;
	       bWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileOutput)));
	       String line = null;
	       int i=0;
	       
	       line = bReader.readLine();
	       bWriter.write(line+",bug_cnt,bug_cnt_avg,LOC_avg,predicted_class"+lineSep);
	       
	       for ( line = bReader.readLine(); line != null && i<bug_cnt.length; line = bReader.readLine()) {
	    	   String predictedClassAttr = line.split(",")[2];
	    	   String predictedClass = predictedClassAttr.split(":")[1];
	    	   bWriter.write(line+","+bug_cnt[i]+","+bug_cnt_avg[i]+","+LOC_avg[i]+","+predictedClass+lineSep);
	    	   i++;
	    }

	    }catch(Exception e){
	        System.out.println(e);
	    }finally  {
	        if(bReader!=null)
	        	bReader.close();
	        if(bWriter!=null)
	        	bWriter.close();
	    }
		
	}

	private static void evaluateSingleInstance() throws Exception {
		
		//Evaluation eval = new Evaluation(train);
		
		int countTrueClsInst = 0;
		int countFalseClsInst = 0;
		
		for (int i = 0; i<test.numInstances(); i++){
			
			Evaluation eval = new Evaluation(train);
			//get one instance
			Instance instance = test.instance(i);
			//evaluate one instance
			double prediction = eval.evaluateModelOnce(cls, instance);
			
			//count predictions: 1.0 or 0.0
			if (prediction == 1.0) countTrueClsInst++;
			else countFalseClsInst++;
			
			//the prediction made by the classifier
			System.out.println("Prediction result: " + prediction);
			System.out.println("Instance index: " + i);
			//print evaluation details for one instance
			System.out.println(eval.toSummaryString());
			System.out.println(eval.toClassDetailsString());
			System.out.println(eval.toMatrixString());
		}
		
		//print predictions = 1.0
		System.out.println("Number of instances - 1.0: " + countTrueClsInst);
		//print predictions = 0.0
		System.out.println("Number of instances - 0.0: " + countFalseClsInst);
		
	}

	private static void crossValidationTest() throws Exception {
		
		Evaluation eval = new Evaluation(train);
		//10 fold cross validation - weka function
		eval.crossValidateModel(cls, test, 10, new Random(1));
		System.out.println(eval.toSummaryString("\nResults\n======\n", true));
		String results = eval.toClassDetailsString("");
		System.out.println(results);
		
	}

	private static void evaluateModel() throws Exception {
		//evaluate classifier and print some statistics
		//Evaluation eval = new Evaluation(train);
	
		//http://weka.8497.n7.nabble.com/Exception-in-thread-quot-main-quot-weka-core-WekaException-No-dataset-structure-provided-td35341.html
		StringBuffer outputBuffer = new StringBuffer();
		CSV output = new CSV();
		output.setHeader(new Instances(train, 0));
		output.setBuffer(outputBuffer);
		File predictFile = new File("predictFile.csv");
		//output.setOutputDistribution(true);
		int LOCindex = train.attribute("LOC").index();
		LOCindex++;
		output.setAttributes(Integer.toString(LOCindex));
		
		output.setOutputFile(predictFile);
		output.printHeader();
		Evaluation eval = new Evaluation(train);
		
		double predictions[] = eval.evaluateModel(cls, test, output);
		output.printFooter();
		
		preformAttributeRanking(train);
		
		//generateROC(eval);
				
		//print evaluation results
		System.out.println(eval.toSummaryString("\nResults\n======\n", true));
		String results = eval.toClassDetailsString("");
		System.out.println("\nResults - classification\n");
		System.out.println(results);
		System.out.println(eval.toMatrixString());
	
		getParameters(results);
		
		System.out.println("ROC Area: " + resultTable.get("ROC-Area-1"));
		String tmp = resultTable.get("ROC-Area-1");
		
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
	    Number number = format.parse(tmp);
	    AUC  = number.doubleValue();
	    
	    String tprate0 = resultTable.get("TP-Rate-0");
	    String tprate1 = resultTable.get("TP-Rate-1");
	    
	    Number number2 = format.parse(tprate0);
	    Number number3 = format.parse(tprate1);
		
		double TPrate_0 = number2.doubleValue();
		double TPrate_1 = number3.doubleValue();
		GM = Math.sqrt(TPrate_0*TPrate_1);
		System.out.println("GM: " + GM);
	}
	
	private static void getParameters(String results) {
		
		//remove white spaces
		resultTable = new Hashtable<String, String>();
		results = results.replaceAll("\\s+","");
		
		//get indexes of class column
		int startStripIndex = results.indexOf("Class");
		int endStripIndex = startStripIndex + "Class".length();
		//remove class column
		results = results.substring(endStripIndex);
		
		//index for every char in result
		int start = 0;
		//index for every column from result
		int titleIndex = 0;
		//array of titles for each column and row from result
		String [] metrics = {"TP-Rate-0", "FP-Rate-0", "Precision-0", "Recall-0", "F-Measure-0", "MCC-0", "ROC-Area-0", "PRC-Area-0",
							 "TP-Rate-1", "FP-Rate-1", "Precision-1", "Recall-1", "F-Measure-1", "MCC-1", "ROC-Area-1", "PRC-Area-1",		
							 "TP-Rate-W", "FP-Rate-W", "Precision-W", "Recall-W", "F-Measure-W", "MCC-W", "ROC-Area-W", "PRC-Area-W"
							};

		//for each char (number)
		for (int i=0; i<results.length(); i++) {
			
			//if end of result string is reached
			if (start+3 > results.length()) break;

			//one number contains 5 characters (0,000) -> start+5
			resultTable.put(metrics[titleIndex], results.substring(start, start+5));
			
			titleIndex++;
			//if end of metrics array is reached
			if (titleIndex == 24) break;
			//remove class data (0)
			if (titleIndex == 8) start = start + 1;
			//remove class data and Weighted Avg. string (1)
			if (titleIndex == 16) start = start + 13;
			start = start + 5;
			
		}
		
	}

	//https://weka.wikispaces.com/file/view/CrossValidationSingleRun.java
	private static void preformCrossValidation(int folds, int seed) throws Exception {
		
		//prepare data for generate outputfile
		StringBuffer outputBuffer = new StringBuffer();
		CSV output = new CSV();
		output.setHeader(new Instances(train, 0));
		output.setBuffer(outputBuffer);
		File predictFile = new File("predictFileCrossValidation.csv");
		output.setOutputDistribution(true);
		//output.setAttributes("1-144");
		output.setOutputFile(predictFile);
		output.printHeader();
		
		// randomize data
	    Random rand = new Random(seed);
	    Instances randData = new Instances(train);
	    // Randomly shuffles the order of instances passed through it
	    randData.randomize(rand);
	    if (randData.classAttribute().isNominal())
	      randData.stratify(folds);
	
		// perform cross-validation
		Evaluation evaluation = new Evaluation(randData);
	    for (int n = 0; n < folds; n++) {
	      
		  Instances trainInstance = randData.trainCV(folds, n);
	      evaluation.setPriors(trainInstance);
	      //Instances testInstance = randData.testCV(folds, n);
	      // the above code is used by the StratifiedRemoveFolds filter, the
	      // code below by the Explorer/Experimenter:
	      // Instances train = randData.trainCV(folds, n, rand);
	
	      // build and evaluate classifier
	      Classifier clsCopy = cls;
	      clsCopy.buildClassifier(trainInstance);
	      
	      evaluation.evaluateModel(clsCopy, test, output);
	      output.printFooter();
	      System.out.println(evaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", true));
		  System.out.println(evaluation.toClassDetailsString());
	    }
	
	}
	
	//https://stackoverflow.com/questions/20014412/weka-only-changing-numeric-to-nominal
	private static Instances filterDataToNominal (Instances originalData) throws Exception {
		
		int column = originalData.attribute("bug_cnt_bin").index();
		column++;
		
	    NumericToNominal convert = new NumericToNominal();
	    String[] options = new String[2];
	    options[0] = "-R";
	    options[1] = Integer.toString(column);
	
	    convert.setOptions(options);
	    convert.setInputFormat(originalData);
	
	    Instances newData = Filter.useFilter(originalData, convert);
	    	    
	    return newData;
	}
	
	//http://weka.wikispaces.com/Use+WEKA+in+your+Java+code
	private static Instances filterDataRemoveAttributes (Instances originalData, String attribute) throws Exception {
		
		int column = 0;
		column = originalData.attribute(attribute).index();
		column++;
		
	    String[] options = new String[2];
	    options[0] = "-R";                                    // "range"
	    options[1] = Integer.toString(column);
	    
	    //System.out.println(options[1]);
	    
	    Remove remove = new Remove();                         // new instance of filter
	    remove.setOptions(options);                           // set options
	    remove.setInputFormat(originalData);                  // inform filter about dataset **AFTER** setting options
	    
	    Instances newData = Filter.useFilter(originalData, remove);
	    
	    return newData;
	}
	
	//https://weka.wikispaces.com/Converting+CSV+to+ARFF
	//convert csv to arff file format
	private static String CSVtoArff(String input) throws IOException  {
		
		Instances data = null;
		
		// load CSV
	    CSVLoader loader = new CSVLoader();
	    loader.setSource(new File(input));
	    data = loader.getDataSet();
	
	    //create output file name
	    String tmp = input.split("\\.", 2)[0];
	    String arffFile = tmp + ".arff";
		    
	    //create output file
	    BufferedWriter writer = new BufferedWriter(new FileWriter(arffFile));
		writer.write(data.toString());

		writer.flush();
	    writer.close();
	    
	    return arffFile;
	}
	
	private static Instances createInstanceFromFile (String arffFileName) throws IOException {
		BufferedReader reader = null;
		reader = new BufferedReader(new FileReader(arffFileName));
		Instances instance = new Instances(reader);
		reader.close();
		return instance;
	}
	
	//https://weka.wikispaces.com/Generating+ROC+curve
	private static void generateROC(Evaluation eval) throws Exception 
	{
		// generate curve
	    ThresholdCurve tc = new ThresholdCurve(); //default 0,5
	    int classIndex = 0; //klasa 0 ili 1
	    Instances result = tc.getCurve(eval.predictions(), classIndex); //Calculates the performance stats for the desired class and return results as a set of Instances.

	    // plot curve
	    ThresholdVisualizePanel vmc = new ThresholdVisualizePanel();
	    vmc.setROCString("(Area under ROC = " +
	        Utils.doubleToString(tc.getROCArea(result), 4) + ")");
	    vmc.setName(result.relationName());
	    PlotData2D tempd = new PlotData2D(result);
	    tempd.setPlotName(result.relationName());
	    tempd.addInstanceNumberAttribute();
	    // specify which points are connected
	    boolean[] cp = new boolean[result.numInstances()];
	    for (int n = 1; n < cp.length; n++)
	      cp[n] = true;
	    tempd.setConnectPoints(cp);
	    // add plot
	    vmc.addPlot(tempd);

	    // display curve
	    String plotName = vmc.getName();
	    final javax.swing.JFrame jf =
	      new javax.swing.JFrame("Weka Classifier Visualize: "+plotName);
	    jf.setSize(500,400);
	    jf.getContentPane().setLayout(new BorderLayout());
	    jf.getContentPane().add(vmc, BorderLayout.CENTER);
	    jf.addWindowListener(new java.awt.event.WindowAdapter() {
	      public void windowClosing(java.awt.event.WindowEvent e) {
	      jf.dispose();
	      }
	    });
	    jf.setVisible(true);
	}
	
	//https://weka.wikispaces.com/Use+Weka+in+your+Java+code#Attribute%20selection
	//http://java-ml.sourceforge.net/content/weka-attribute-selection
	//https://stackoverflow.com/questions/21267988/how-to-rank-features-by-their-importance-in-a-weka-classifier
	//https://www.programcreek.com/java-api-examples/index.php?api=weka.filters.supervised.attribute.AttributeSelection
	private static void preformAttributeRanking (Instances instances) throws Exception 
	{
		InfoGainAttributeEval eval = new InfoGainAttributeEval();
		eval.buildEvaluator(instances);
		
		Map<Attribute, Double> infogainscores = new HashMap<Attribute, Double>();
		for (int i = 0; i < instances.numAttributes(); i++) {
		    Attribute t_attr = instances.attribute(i);
		    double infogain  = eval.evaluateAttribute(i);
		    infogainscores.put(t_attr, infogain);
		}
		
		entriesSortedByValues(infogainscores, -1);
	}
	
	private static <V extends Comparable<? super V>> int compareToRetainDuplicates(V v1, V v2) {
	    return (v1.compareTo(v2) == -1) ? -1 : 1;
	}
	
	 /**
	  * Provides a {@code SortedSet} of {@code Map.Entry} objects. The sorting is in ascending order if {@param order} > 0
	  * and descending order if {@param order} <= 0.
	  * @param map   The map to be sorted.
	  * @param order The sorting order (positive means ascending, non-positive means descending).
	  * @param <K>   Keys.
	  * @param <V>   Values need to be {@code Comparable}.
	  * @return      A sorted set of {@code Map.Entry} objects.
	 * @throws FileNotFoundException 
	  */
	 static <K,V extends Comparable<? super V>> SortedSet<Map.Entry<K,V>>
	 entriesSortedByValues(Map<K,V> map, final int order) throws FileNotFoundException {
	     SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<>(
	         new Comparator<Map.Entry<K,V>>() {
	             public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
	                 return (order > 0) ? compareToRetainDuplicates(e1.getValue(), e2.getValue()) : compareToRetainDuplicates(e2.getValue(), e1.getValue());
	         }
	     }
	    );
	    sortedEntries.addAll(map.entrySet());
	    
	    //create buffer for entropy values
	    tmpBuffer = new StringBuilder();
	    tmpBuffer.append("key");
	    tmpBuffer.append(',');
	    tmpBuffer.append("value");
	    tmpBuffer.append('\n');
	    
	    for (Map.Entry<K, V> entry : sortedEntries) {
	        //System.out.println("Key: " + entry.getKey() + ". Value: " + entry.getValue());
	        writeToCsv(entry.getKey().toString(), entry.getValue().toString());
	   }
	   
	   //add buffer to writer and make infoGainAttributeEval.csv
	   PrintWriter writerForCsv = new PrintWriter(new File("infoGainAttributeEval.csv"));
	   writerForCsv.write(tmpBuffer.toString());
	   writerForCsv.close();
	    
	   return sortedEntries;
	}

	private static void writeToCsv(String key, String value) {
		
		tmpBuffer.append(key);
		tmpBuffer.append(',');
		tmpBuffer.append(value);
		tmpBuffer.append('\n');
		
	}
	
	private static double[] dataValuesAsArray(Instances data, String attribute) { 
        double dataArray[] = new double[data.numInstances()]; 
        for (int i = 0; i < data.numInstances(); i++) { 
        	dataArray[i] = data.instance(i).value(data.attribute(attribute).index()); 
        } 
        return dataArray; 
    }
	
	private static double[] dataToCumulativeSum(double[] data) 
	{
		double[] cumulativeData = new double[data.length];
		double tmp =  data[0];
		cumulativeData[0] = tmp;
		
		for (int i = 1; i < data.length; i++) {
			double sum = tmp + data[i];
			cumulativeData[i] = sum;
			tmp = cumulativeData[i];			
		}
		
		return cumulativeData;
	}
	
	private static void showCurveData (String model, String filename) throws Exception
	{
		int[] sortingOrder = {0, 0};
		String title = "Optimal";
		
		if (model.equals("worst"))
		{
			sortingOrder[0] = 1; 
			sortingOrder[1] = 1; 
			title = "Worst";
		} else if (model.equals("predicted"))
		{
			sortIndexes[0] = 9;
			sortIndexes[1] = 5;
			sortingOrder[0] = 0; 
			sortingOrder[1] = 1; 
			title = "Predicted";
		}
		
	    String[] filenames = {filename};
		MultiColumnCsvSortExtended.main(filenames, sortIndexes, sortingOrder);
		
		//test release file name
		String arffFile_n = CSVtoArff(filename + "_sorted.csv");
		
		//make test instance
		Instances testInstance = createInstanceFromFile(arffFile_n);
		
		double[] bug_cnt_avg = dataValuesAsArray(testInstance, "bug_cnt_avg");
		double[] loc_avg = dataValuesAsArray(testInstance, "LOC_avg");
		
		double[] bug_cnt_avg_cumulative = dataToCumulativeSum(bug_cnt_avg);
		double[] loc_avg_cumulative = dataToCumulativeSum(loc_avg);
		
		areaUnderCurve(loc_avg_cumulative, bug_cnt_avg_cumulative, model);
		
		String[] name = {title}; 
		PlotCurve.main(name, loc_avg_cumulative, bug_cnt_avg_cumulative);
		
	}
	
}
