package weka_test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//https://stackoverflow.com/questions/26448813/how-to-sort-csv-file-by-two-columns-in-java
public class MultiColumnCsvSortExtended
{
    private static final String COLUMN_SEPARATOR = ",";
    private static String filename;

    public static void main(String[] args, int[] sortingIndexes, int[] sortingOrder) throws Exception
    {
    	
    	if (args.length < 1) {
	      System.out.println("\nPlease enter name of the file(s) you want to sort.\n");
	      System.exit(1);
	    }
    	
    	for (int i = 0; i < args.length; i++)
    	{
    		filename = args[i] + ".csv";
        	String outputFileName = args[i] + "_sorted.csv";
        	
            InputStream inputStream = new FileInputStream(filename);
            List<List<String>> lines = readCsv(inputStream);

            // Create a comparator that compares the elements from column sortingIndexes[0]
            // in descending order
            // column count starts at 0
            Comparator<List<String>> c0 = null;
            if (sortingOrder[0] == 0)
            {
            	c0 = createDesendingComparator(sortingIndexes[0]);
            }
            else if (sortingOrder[0] == 1)
            {
            	c0 = createAscendingComparator(sortingIndexes[0]);
            }

            // Create a comparator that compares the elements from column sortingIndexes[1]
            // in descending order
            Comparator<List<String>> c1 = null; 
            if (sortingOrder[1] == 0)
            {
            	c1 = createDesendingComparator(sortingIndexes[1]);
            }
            else if (sortingOrder[1] == 1)
            {
            	c1 = createAscendingComparator(sortingIndexes[1]);
            }

            // Create a comparator that compares primarily by using c0,
            // and secondarily by using c1
            Comparator<List<String>> comparator = createComparator(c0, c1);
            Collections.sort(lines, comparator);

            OutputStream outputStream = new FileOutputStream(outputFileName);
            String header = readHeader();
            writeCsv(header, lines, outputStream);        
    		
    	}
    }
    
    private static String readHeader() throws IOException
    {
    	InputStream inputStream = new FileInputStream(filename);
    	BufferedReader reader = new BufferedReader(
                new InputStreamReader(inputStream));
    	
    	String header = reader.readLine();
    	return header;
    }

    private static List<List<String>> readCsv(
        InputStream inputStream) throws IOException
    {
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(inputStream));
        List<List<String>> lines = new ArrayList<List<String>>();

        String line = null;

        // Skip header
        line = reader.readLine();

        while (true)
        {
            line = reader.readLine();
            if (line == null)
            {
                break;
            }
            List<String> list = Arrays.asList(line.split(COLUMN_SEPARATOR));
            lines.add(list);
        }
        return lines;
    }

    private static void writeCsv(
        String header, List<List<String>> lines, OutputStream outputStream) 
        throws IOException
    {
        Writer writer = new OutputStreamWriter(outputStream);
        writer.write(header+"\n");
        for (List<String> list : lines)
        {
            for (int i = 0; i < list.size(); i++)
            {
                writer.write(list.get(i));
                if (i < list.size() - 1)
                {
                    writer.write(COLUMN_SEPARATOR);
                }
            }
            writer.write("\n");
        }
        writer.close();

    }

    @SafeVarargs
    private static <T> Comparator<T>
        createComparator(Comparator<? super T>... delegates)
    {
        return (t0, t1) -> 
        {
            for (Comparator<? super T> delegate : delegates)
            {
                int n = delegate.compare(t0, t1);
                if (n != 0)
                {
                    return n;
                }
            }
            return 0;
        };
    }

    private static <T extends Comparable<? super T>> Comparator<List<T>>
        createAscendingComparator(int index)
    {
    	return (list0, list1) ->
	    {
	    	float change1 = Float.valueOf((String) list0.get(index));
	    	float change2 = Float.valueOf((String) list1.get(index));

	    	if (change1 < change2) return -1;
	    	if (change1 > change2) return 1;
	    	return 0;
	    	//return Float.compare(change1, change2);
	    };
    }

    private static <T extends Comparable<? super T>> Comparator<List<T>>
        createDesendingComparator(int index)
    {
    	return (list0, list1) ->
	    {
	    	float change1 = Float.valueOf((String) list0.get(index));
	    	float change2 = Float.valueOf((String) list1.get(index));

	    	if (change1 < change2) return 1;
	    	if (change1 > change2) return -1;
	    	return 0;
	    	//return Float.compare(change1, change2);
	    };
    }

    /*private static <T> Comparator<List<T>>
    createListAtIndexComparator(Comparator<? super T> delegate, int index)
	{
	    return (list0, list1) ->
	    {
	    	float change1 = Float.valueOf((String) list0.get(index));
	    	float change2 = Float.valueOf((String) list1.get(index));

	    	if (change1 < change2) return -1;
	    	if (change1 > change2) return 1;
	    	return 0;
	    	//return Float.compare(change1, change2);
	    };
	}*/

}