package weka_test;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class PlotCurve extends ApplicationFrame 
{
	public PlotCurve(final String title, double[] x, double[] y) {

	    super(title);
	    final XYSeries series = new XYSeries(title);
	   
	    for (int i = 0; i < x.length; i++)
	    {
	    	series.add(x[i], y[i]);
	    }
	    
	    final XYSeriesCollection data = new XYSeriesCollection(series);
	    final JFreeChart chart = ChartFactory.createXYLineChart(
	    	title,
	        "X", 
	        "Y", 
	        data,
	        PlotOrientation.VERTICAL,
	        true,
	        true,
	        false
	    );

	    final ChartPanel chartPanel = new ChartPanel(chart);
	    chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
	    setContentPane(chartPanel);

	}
	
	public static void main(final String[] args, double[] x, double[] y) 
	{
	    final PlotCurve demo = new PlotCurve(args[0], x, y);
	    demo.pack();
	    RefineryUtilities.centerFrameOnScreen(demo);
	    demo.setVisible(true);
	}
}
